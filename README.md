# Met Museum

[Repository du projet Met Museum](https://gitlab.com/SamiraTesse/react-project)

Ce dépôt héberge les différents dossiers du projet contenant les fonctionnalités de ce dernier.

## Installation des outils

### Prérequis

Avant de commencer, assurez-vous d'avoir installé un IDE (Environnement de Développement Intégré) pour ReactJS. Nous vous recommandons IntelliJ, mais vous êtes libre de choisir celui de votre choix.

### Installation de Node.js et NPM

#### Pour Windows

1. Téléchargez l'installateur Windows depuis [Node.js](https://nodejs.org/en/download/). Il inclut le gestionnaire de paquets NPM. Choisissez la dernière version ou celle dont vous avez besoin.

2. Double-cliquez sur le fichier `.msi` pour démarrer le processus d'installation. Suivez les invites et choisissez le chemin où vous souhaitez installer Node.js et NPM.

3. Vérifiez les versions de Node.js et NPM en exécutant les commandes suivantes dans une invite de commande :
    ```shell
    node -v 
    npm -v 
    ```

#### Pour Linux

1. Ouvrez un terminal et exécutez les commandes suivantes pour installer Node.js et NPM via le gestionnaire de paquets `apt` :
    ```shell
    sudo apt update
    sudo apt install nodejs npm
    ```

2. Vérifiez les versions de Node.js et NPM :
    ```shell
    node -v 
    npm -v 
    ```

#### Pour macOS

1. Téléchargez le fichier `.pkg` depuis [Node.js](https://nodejs.org/en/download/).

2. Exécutez le programme d'installation en double-cliquant sur le fichier téléchargé.

3. Vérifiez l'installation de Node.js :
    ```shell
    node -v 
    ```

4. Mettez à jour votre version de NPM :
    ```shell
    sudo npm install npm --global
    ```

### Installation de ViteJS

1. Installez ViteJS en utilisant NPM :
    ```shell
    npm install -g create-vite
    ```

2. Créez un nouveau projet Vite :
    ```shell
    npx create-vite@latest mon-projet
    cd mon-projet
    npm install
    ```

### Installation de Tailwind CSS

#### Pour Windows, Linux, et macOS

1. Installez Tailwind CSS via NPM :
    ```shell
    npm install -D tailwindcss
    ```

2. Initialisez la configuration Tailwind CSS :
    ```shell
    npx tailwindcss init
    ```

3. Configurez Tailwind CSS en ajoutant les chemins vers tous vos fichiers dans le fichier `tailwind.config.js` :
    ```javascript
    module.exports = {
      content: [
        "./index.html",
        "./src/**/*.{js,ts,jsx,tsx}",
      ],
      theme: {
        extend: {},
      },
      plugins: [],
    }
    ```

4. Ajoutez les directives Tailwind dans votre fichier CSS :
    ```css
    @tailwind base;
    @tailwind components;
    @tailwind utilities;
    ```

## Clonage du dépôt distant

Une fois chaque outil installé sur votre ordinateur, vous pouvez cloner votre dépôt distant dans votre environnement local à l'aide des commandes Git dans le dossier de votre choix :

```shell
cd chemin/vers/votre/dossier
git clone https://gitlab.com/SamiraTesse/react-project.git
```

## Démarrage du Met Museum

À présent, vous pouvez lancer l'application en exécutant la commande suivante :

```shell
npm run dev
```
