import React from 'react';
import Team from '../../Team/team';

const About: React.FC = () => {
    return (
        <div className="container mx-auto p-4 mt-5">
            <h1 className="text-4xl font-semibold text-red-900 mb-4">About Us</h1>
            <div className="bg-yellow-50 p-6 rounded-lg shadow-md">
                <h2 className="text-2xl font-semibold mb-2 text-red-800">Our Mission</h2>
                <p className="text-gray-800 mb-4">
                    Our mission is to provide access to a vast collection of art from all around the world. We aim to inspire, educate, and connect people through the power of art.
                </p>
                <h2 className="text-2xl font-semibold mb-2 text-red-800">Our Vision</h2>
                <p className="text-gray-800 mb-4">
                    We envision a world where everyone has the opportunity to experience and appreciate art. We strive to create a platform that makes art accessible to all.
                </p>
                <h2 className="text-2xl font-semibold mb-2 text-red-800">Our History</h2>
                <p className="text-gray-800 mb-4">
                    Founded in 2024, SupKnowledge Art Collection has grown from a small initiative to a comprehensive platform hosting thousands of artworks. Our dedication to preserving and promoting art has driven our continuous growth and success.
                </p>
            </div>
            <div className='mt-5'>
            <Team />
            </div>
        </div>
    );
};

export default About;
