import React, { useState } from 'react';
import SearchBar from '../../SearchBar/searchbar';
import Heros from '../../Heros/heros';
import Collection from '../../Collections/collections';


const HomePage: React.FC = () => {
    const [searchQuery, setSearchQuery] = useState('');

    const handleSearch = (query: string) => {
        setSearchQuery(query);
    };

    return (
        <>
            <div>
                <SearchBar onSearch={handleSearch} />
            </div>

            <div className="container mx-auto p-4 bg-yellow-50 w-full">
                <Heros />
            </div>

            <div className="container mx-auto p-4 mt-5">
                <h1 className="lg:text-4xl text-2xl text-red-900 font-semibold">
                    Our Highlights
                </h1>
                <p className='mt-5'>
                    Our collections that are currently all the rage. To find out more, click the 'See More' button.
                </p>
                <div className="mt-5">
                    <Collection searchQuery={searchQuery} numCollections={4} />
                </div>
            </div>
        </>
    );
};

export default HomePage;
