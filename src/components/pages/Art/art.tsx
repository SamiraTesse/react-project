import { useState } from "react";
import Collection from "../../Collections/collections";
import SearchBar from "../../SearchBar/searchbar";

export default function Art() {

    const [searchQuery, setSearchQuery] = useState('');

    const handleSearch = (query: string) => {
        setSearchQuery(query);
    };
    
    return (
        <div className="container mx-auto p-4 bg-yellow-50 w-full">
            <h1 className="lg:text-4xl text-2xl text-red-900 font-semibold">
                Our Pieces
            </h1>
            <SearchBar onSearch={handleSearch} />
            
            <Collection searchQuery={searchQuery} numCollections={16}/>
           
        </div>
    )
}