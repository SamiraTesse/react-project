import React from "react";
import { Link } from "react-router-dom";
import Image from "../Image/image";
import Button from "../Button/button";

interface CardProps {
  id: number;
  title: string;
  description: string;
  image: string;
  artist: string;
}

const Card: React.FC<CardProps> = ({
  id,
  title,
  description,
  image,
  artist,
}) => {
  return (
    <div className="w-full max-w-xs overflow-hidden rounded-lg shadow-lg bg-yellow-50 m-4">
      <Image src={image} width="w-full" height="h-56" alt={title} />
      <div className="py-5 text-center">
        <h3 className="block text-xl font-bold text-gray-800 mb-2">{title}</h3>
        <p className="text-sm text-gray-700 mb-4">{description}</p>
        <span className="text-sm text-gray-700 block mb-4">{artist}</span>
        <Link to={`/detail/${id}`} className="inline-block mb-4">
          <Button id={id} />
        </Link>
      </div>
    </div>
  );
};

export default Card;
