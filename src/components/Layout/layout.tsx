import React from 'react';
import Header from '../Header/header.tsx';
import Navbar from "../NavBar/navbar.tsx";
import Footer from "../Footer/footer.tsx";


interface LayoutProps {
    children: React.ReactNode;
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
    return (
        <div className="flex flex-col min-h-screen">
            <Header>
                <Navbar />
            </Header>
            <div className="flex flex-1 p-5 bg-amber-100">
                <main className="flex-1 bg-amber-100 ">
                    {children}
                </main>
            </div>
            <Footer />
        </div>
    );
};

export default Layout;
