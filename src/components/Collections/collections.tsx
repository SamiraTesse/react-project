import React, { useState, useEffect } from 'react';
import Card from "../Cards/cards.tsx";
import Image from '../Image/image.tsx';

interface CollectionProps {
    searchQuery: string;
    numCollections: number;
}

interface Collection {
    objectID: number;
    title: string;
    artistDisplayName: string;
    primaryImage: string;
    objectDate: string;
}

const Collection: React.FC<CollectionProps> = ({ searchQuery, numCollections }) => {
    const [collections, setCollections] = useState<Collection[]>([]);
    const [loading, setLoading] = useState<boolean>(true);

    useEffect(() => {
        fetchCollections();
    }, [numCollections]);

    const fetchCollections = async () => {
        try {
            const response = await fetch('https://collectionapi.metmuseum.org/public/collection/v1/objects');
            const data = await response.json();

            const validObjects: Collection[] = [];
            let count = 0;

            for (const id of data.objectIDs) {
                if (count >= numCollections) break;

                const objectResponse = await fetch(`https://collectionapi.metmuseum.org/public/collection/v1/objects/${id}`);
                const objectData = await objectResponse.json();

                if (objectData.primaryImage && objectData.title && objectData.artistDisplayName) {
                    validObjects.push({
                        objectID: objectData.objectID,
                        title: objectData.title,
                        artistDisplayName: objectData.artistDisplayName,
                        primaryImage: objectData.primaryImage,
                        objectDate: objectData.objectDate,
                    });
                    count++;
                }
            }

            setCollections(validObjects);
        } catch (error) {
            console.error("Failed to fetch collections:", error);
        } finally {
            setLoading(false);
        }
    };

    const filteredCollections = collections.filter(collection =>
        collection.title.toLowerCase().includes(searchQuery.toLowerCase()) ||
        collection.artistDisplayName.toLowerCase().includes(searchQuery.toLowerCase())
    );

    if (loading) {
        return (
            <div className="flex flex-col items-center justify-center min-h-screen mt-2">
                <Image src="assets/loading.svg" alt="Loading..." width={''} height={''} />
                <p className="text-xl text-gray-700">Loading collections, please wait...</p>
            </div>
        );
    }

    return (
        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
            {filteredCollections.map((collection) => (
                <Card
                    key={collection.objectID}
                    id={collection.objectID}
                    title={collection.title}
                    description={collection.objectDate}
                    image={collection.primaryImage}
                    artist={collection.artistDisplayName}
                />
            ))}
        </div>
    );
};

export default Collection;
