import React from "react";
import Image from "../Image/image";

const Team: React.FC = () => {
  return (
    <section className="rounded-lg shadow-lg bg-yellow-50 ">
      <div className="container px-6 py-10 mx-auto">
        <div className="xl:flex xl:items-center xl:-mx-4">
          <div className="xl:w-1/2 xl:mx-4">
            <h1 className="text-2xl font-semibold text-red-800 capitalize lg:text-3xl">Our Team</h1>
            <p className="max-w-2xl mt-4 text-gray-500">
              At SupKnowledge Art Collection, we are fortunate to have a dedicated and talented team. Here’s a look at two key members who have significantly contributed to our project.
            </p>
          </div>
          <div className="grid grid-cols-1 gap-8 mt-8 xl:mt-0 xl:mx-4 xl:w-1/2 md:grid-cols-2">
            <div className="flex flex-col items-center">
              <Image
                src="https://images.unsplash.com/photo-1570295999919-56ceb5ecca61?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80"
                alt="Nelson SANA" 
                width={""} 
                height={""}              
              />
              <h1 className="mt-4 text-2xl font-semibold text-gray-700 capitalize">Nelson SANA</h1>
              <p className="mt-2 text-gray-500 capitalize">Full stack developer</p>
            </div>
            <div className="flex flex-col items-center">
              <Image
                src="https://images.unsplash.com/photo-1499470932971-a90681ce8530?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80"
                alt="Samira TESSÉ"
                width={""} 
                height={""}  
              />
              <h1 className="mt-4 text-2xl font-semibold text-gray-700 capitalize">Samira TESSÉ</h1>
              <p className="mt-2 text-gray-500 capitalize">Graphic Designer</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Team;
