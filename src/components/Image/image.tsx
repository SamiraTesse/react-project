import React from 'react';

interface ImageProps {
    src: string;
    width: string;
    height: string;
    alt?: string;
}

const Image: React.FC<ImageProps> = ({ src, width, height, alt }) => {
    return (
        <img
            src={src}
            alt={alt || 'image'}
            className={`object-cover ${width} ${height}`}
        />
    );
};

export default Image;
