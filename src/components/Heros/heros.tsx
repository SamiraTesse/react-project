import { Component} from 'react';
import Image from "../Image/image.tsx";


class Heros extends Component {
    render() {
        return (
            <div className="flex flex-col lg:flex-row lg:h-[32rem]">
                <div className="flex items-center justify-center w-full px-6 py-8 lg:w-1/2">
                    <div className="max-w-xl">
                        <h2 className="text-3xl font-semibold text-gray-800 lg:text-4xl">
                            Welcome to <span className="text-red-900">SupKnowledge!</span>
                        </h2>
                        <p className="mt-4 text-sm text-gray-400 lg:text-base">
                            Explore our new interface dedicated to artifacts and art collections. Designed for researchers and art enthusiasts, it offers quick access to valuable information and allows you to discover the richness of our cultural heritage.
                        </p>
                    </div>
                </div>
                <div className="relative w-full h-64 lg:w-1/2 lg:h-auto">
                    <Image
                        src="/assets/art.png"
                        width="w-full"
                        height="h-full"
                        alt="Heros image"
                    />
                </div>
            </div>
        );
    }
}

export default Heros;