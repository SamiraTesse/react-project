import React from "react";
import { useNavigate } from "react-router-dom";

interface ButtonProps {
  id: number;
}

const Button: React.FC<ButtonProps> = ({ id }) => {
  const navigate = useNavigate();

  const handleViewDetails = () => {
    navigate(`/details/${id}`);
  };

  return (
    <button
      onClick={handleViewDetails}
      className="mt-4 px-4 py-2 bg-red-800 text-white rounded hover:bg-blue-700"
    >
      See More
    </button>
  );
};

export default Button;
