import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import Image from './Image/image';

interface CollectionDetail {
    objectID: number;
    title: string;
    artistDisplayName: string;
    primaryImage: string;
    objectDate: string;
    medium: string;
    dimensions: string;
    creditLine: string;
    additionalImages: string[];
}

const DetailedView: React.FC = () => {
    const { id } = useParams<{ id: string }>();
    const [collectionDetail, setCollectionDetail] = useState<CollectionDetail | null>(null);
    const [loading, setLoading] = useState<boolean>(true);

    useEffect(() => {
        fetchCollectionDetail();
    }, [id]);

    const fetchCollectionDetail = async () => {
        try {
            const response = await fetch(`https://collectionapi.metmuseum.org/public/collection/v1/objects/${id}`);
            const data = await response.json();
            setCollectionDetail(data);
        } catch (error) {
            console.error("Failed to fetch collection detail:", error);
        } finally {
            setLoading(false);
        }
    };

    if (loading) {
        return (
            <div className="flex flex-col items-center justify-center min-h-screen">
                <Image src="/assets/loading.svg" alt="Loading..." width={''} height={''} />
                <p className="text-xl text-gray-700">Loading collection details, please wait...</p>
            </div>
        );
    }

    if (!collectionDetail) {
        return (
            <div className="flex flex-col items-center justify-center min-h-screen">
                <p className="text-xl text-gray-700">Collection not found</p>
            </div>
        );
    }

    return (
        <div className="container mx-auto p-4">
            <div className="bg-yellow-50 p-6 rounded-lg shadow-md">
                <Image src={collectionDetail.primaryImage} alt={collectionDetail.title} width={''} height={''} />
                <h1 className="text-4xl font-semibold text-red-900 mb-4">{collectionDetail.title}</h1>
                <p className="text-gray-700 mb-2">Artist: {collectionDetail.artistDisplayName}</p>
                <p className="text-gray-700 mb-2">Date: {collectionDetail.objectDate}</p>
                <p className="text-gray-700 mb-2">Medium: {collectionDetail.medium}</p>
                <p className="text-gray-700 mb-2">Dimensions: {collectionDetail.dimensions}</p>
                <p className="text-gray-700 mb-2">Credit Line: {collectionDetail.creditLine}</p>
            </div>
        </div>
    );
};

export default DetailedView;
