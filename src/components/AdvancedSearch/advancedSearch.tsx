import React, { useState } from 'react';

interface AdvancedSearchBarProps {
    onSearch: (query: string, artist: string, date: string) => void;
}

const AdvancedSearchBar: React.FC<AdvancedSearchBarProps> = ({ onSearch }) => {
    const [query, setQuery] = useState('');
    const [artist, setArtist] = useState('');
    const [date, setDate] = useState('');

    const handleSearch = () => {
        onSearch(query, artist, date);
    };

    return (
        <div className="flex flex-col space-y-2 md:flex-row md:space-y-0 md:space-x-2 mb-4">
            <input
                type="text"
                placeholder="Search by title"
                value={query}
                onChange={(e) => setQuery(e.target.value)}
                className="p-2 border border-gray-300 rounded"
            />
            <input
                type="text"
                placeholder="Search by artist"
                value={artist}
                onChange={(e) => setArtist(e.target.value)}
                className="p-2 border border-gray-300 rounded"
            />
            <input
                type="text"
                placeholder="Search by date"
                value={date}
                onChange={(e) => setDate(e.target.value)}
                className="p-2 border border-gray-300 rounded"
            />
            <button onClick={handleSearch} className="p-2 bg-red-800 text-white rounded">
                Search
            </button>
        </div>
    );
};

export default AdvancedSearchBar;
