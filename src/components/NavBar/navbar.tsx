import React, { useState } from 'react';
import { Link } from "react-router-dom";

const Navbar: React.FC = () => {
    const [isOpen, setIsOpen] = useState(false);

    const toggleMenu = () => {
        setIsOpen(!isOpen);
    };

    return (
        <nav className="bg-yellow-50 h-12 shadow-md flex items-center justify-between px-4 md:px-20">
            <div className="flex items-center space-x-2">
                <a href="#">
                    <img
                        src="https://avatars.githubusercontent.com/u/13870646?s=200&v=4"
                        alt="Brand Logo"
                        className="w-8 h-8 cursor-pointer"
                    />
                </a>
                <span className="text-red-800 font-bold">Museum</span>
            </div>
            <div className="md:hidden">
                <button
                    onClick={toggleMenu}
                    className="text-red-800 focus:outline-none"
                >
                    <svg
                        className="w-6 h-6"
                        fill="none"
                        stroke="currentColor"
                        viewBox="0 0 24 24"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        {isOpen ? (
                            <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                d="M6 18L18 6M6 6l12 12"
                            />
                        ) : (
                            <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                d="M4 6h16M4 12h16M4 18h16"
                            />
                        )}
                    </svg>
                </button>
            </div>
            <div className={`flex-col md:flex md:flex-row md:space-x-12 text-red-800 ${isOpen ? 'flex' : 'hidden'} absolute md:relative top-12 left-0 md:top-0 md:left-0 w-full md:w-auto bg-yellow-50 md:bg-transparent z-10`}>
                <Link to="/" className="hover:bg-yellow-300 p-2 rounded transition-colors duration-300">Home</Link>
                <Link to="/art" className="hover:bg-yellow-300 p-2 rounded transition-colors duration-300">Art</Link>
                <Link to="/about" className="hover:bg-yellow-300 p-2 rounded transition-colors duration-300">About</Link>
            </div>
        </nav>
    );
};

export default Navbar;
