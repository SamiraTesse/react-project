import React from 'react';

const Footer: React.FC = () => {
    return (
        <footer className="bg-yellow-50 text-black-800 py-6">
            <div className="container mx-auto px-4 text-center md:text-left">
                <div className="flex flex-col md:flex-row justify-between items-center">
                    <p className="mb-4 md:mb-0">&copy; 2024 SupKnowledge Art Collection. All rights reserved.</p>
                    <div className="flex space-x-4">
                        <a href="/privacy-policy" className="hover:underline">Privacy Policy</a>
                        <a href="/terms-of-service" className="hover:underline">Terms of Service</a>
                        <a href="/contact" className="hover:underline">Contact</a>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default Footer;
