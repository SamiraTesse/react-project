import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import Layout from "./components/Layout/layout.tsx";
import {Route, BrowserRouter as Router, Routes} from 'react-router-dom';
import HomePage from "./components/pages/HomePage/homePage.tsx";
import Art from "./components/pages/Art/art.tsx";
import DetailedView from './components/detailedView.tsx';
import About from './components/pages/About/About.tsx';


ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
      <Router>
          <Layout>
              <Routes>
                  <Route path="/"  element={<HomePage/>}/>
                  <Route path="/art" element={<Art/>} />
                  <Route path="/about"  element={<About />}/>
                  <Route path="/detail/:id" element={<DetailedView />} />
              </Routes>
          </Layout>
      </Router>
  </React.StrictMode>,
)
